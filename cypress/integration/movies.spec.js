context('Movies', () => {
    beforeEach(() => {
        cy.visit('http://localhost:4200/movies');
    });
    it('Try to add a movie with a name less than 3 characters, add button should be disabled', () => {
        cy.get('#saveBtn').should('be.disabled');
        cy.get('#name').type('St', { delay: 100 })
            .should('have.value', 'St');
        cy.get('#saveBtn').should('be.disabled');
    
    });
    it('Try to add a movie with a name greater than 2 characters, add button should be enabled and add the movie after click it', () => {
        cy.get('#name').type('Sta', { delay: 100 })
            .should('have.value', 'Sta');
        cy.get('#saveBtn').should('be.enabled');
        cy.get('#saveBtn').click();
    });
});