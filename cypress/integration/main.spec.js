context('MainApp', () => {
    beforeEach(() => {
        cy.visit('http://localhost:4200');
    });
    it('Change to french language should change welcome sentence', () => {
        cy.get('#languageSelector').select('fr');
        cy.get('h3').contains('Bonjour');
    });
    it('Change to movies menu', () => {
        cy.get('#nav_movies').click();
        cy.get('h3').contains('Movies');
    });
})