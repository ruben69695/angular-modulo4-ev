import { Component } from '@angular/core';
import { Router, NavigationEnd, Event } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { TodoMoviesApiClient } from './models/todo-movies-api-client.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [TodoMoviesApiClient]
})
export class AppComponent {
  title = 'todo-app';

  constructor(private router: Router, private translate: TranslateService, private api: TodoMoviesApiClient) {
    router.events.subscribe( (event: Event) => {
      if (event instanceof NavigationEnd) {

        const navHome = document.getElementById('nav_home');
        const navHistory = document.getElementById('nav_history');
        const navMovies = document.getElementById('nav_movies');
        const navLogin = document.getElementById('nav_login');

        switch (event.urlAfterRedirects) {
          case '/home': {
            navHome.classList.add('active');
            navHistory.classList.remove('active');
            navMovies.classList.remove('active');
            navLogin.classList.remove('active');
            break;
          }
          case '/history': {
            navHome.classList.remove('active');
            navHistory.classList.add('active');
            navMovies.classList.remove('active');
            navLogin.classList.remove('active');
            break;
          }
          case '/movies/main': {
            navHome.classList.remove('active');
            navHistory.classList.remove('active');
            navMovies.classList.add('active');
            navLogin.classList.remove('active');
            break;
          }
          case '/login': {
            navHome.classList.remove('active');
            navHistory.classList.remove('active');
            navMovies.classList.remove('active');
            navLogin.classList.add('active');
            break;
          }
        }
      }
    });
  }

  getTranslations(lang: string) {
    this.translate.use(lang);
  }

  getTags(): number {
    return this.api.getAllTags();
  }

}
