import { 
    TodoMoviesActionTypes,
    AddTodoMovieAction,
    InitDataTodoMovieAction,
    VoteDownTodoMovieAction,
    VoteUpTodoMovieAction,
    AddTagCountAction
} from './movies-items.actions';
import { TodoMovieItem } from 'src/app/models/todo-movie-item.models';

describe('VoteUpTodoMovieAction', () => {
    it('should have the correcte action type', () => {
        const voteUpAction = new VoteUpTodoMovieAction(null);
        expect(voteUpAction.type).toEqual(TodoMoviesActionTypes.VOTE_UP);
    });
});

describe('VoteDownTodoMovieAction', () => {
    it('should have the correcte action type', () => {
        const voteDowmAction = new VoteDownTodoMovieAction(null);
        expect(voteDowmAction.type).toEqual(TodoMoviesActionTypes.VOTE_DOWN);
    });
});

describe('InitDataTodoMovieAction', () => {
    it('should have the correcte action type', () => {
        const InitAction = new InitDataTodoMovieAction(null);
        expect(InitAction.type).toEqual(TodoMoviesActionTypes.INIT_DATA);
    });
});

describe('AddTodoMovieAction', () => {
    it('should have the correcte action type', () => {
        const AddNewAction = new AddTodoMovieAction(null);
        expect(AddNewAction.type).toEqual(TodoMoviesActionTypes.ADD_MOVIE);
    });
});

describe('AddTagCountAction', () => {
    it('should have the correcte action type', () => {
        const addTagAction = new AddTagCountAction(5);
        expect(addTagAction.type).toEqual(TodoMoviesActionTypes.ADD_TAG_COUNT);
    });
});