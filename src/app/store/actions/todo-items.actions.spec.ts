import { TodoItemsActionTypes, NewTodoItemAction, DeleteTodoItemsAction } from './todo-items.actions';


describe('NewTodoItemAction', () => {
    it('should have the correct action type', () => {
        const newTodoItemAction = new NewTodoItemAction(null);
        expect(newTodoItemAction.type).toEqual(TodoItemsActionTypes.NEW_TODO_ITEM);
    });
});

describe('DeleteTodoItemsAction', () => {
    it('should have the correct action type', () => {
        const deleteTodoItemAction = new DeleteTodoItemsAction();
        expect(deleteTodoItemAction.type).toEqual(TodoItemsActionTypes.DELETE_TODO_ITEMS);
    });
});



