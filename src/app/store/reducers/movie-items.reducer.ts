import { TodoMovieItem } from 'src/app/models/todo-movie-item.models';
import { TodoMoviesActions, TodoMoviesActionTypes, VoteUpTodoMovieAction,
    VoteDownTodoMovieAction,
    InitDataTodoMovieAction,
    AddTodoMovieAction,
    AddTagCountAction} from '../actions/movies-items.actions';

// STATES
export interface TodoMoviesState {
    items: TodoMovieItem[];
    tagCount: number;
}

export const initializeTodoMoviesState: TodoMoviesState = {
    items: [],
    tagCount: 0
};

// REDUCERS
export function reducerTodoMovies(state: TodoMoviesState = initializeTodoMoviesState, action: TodoMoviesActions): TodoMoviesState {
    switch (action.type) {
        case TodoMoviesActionTypes.INIT_DATA:
            const destinos: TodoMovieItem[] = (action as InitDataTodoMovieAction).movies;
            return {
                ...state,
                items: destinos.map((x) => new TodoMovieItem(x.name, x.creationDate, x.description)),
                tagCount: 0
            };
        case TodoMoviesActionTypes.VOTE_UP:
            (action as VoteUpTodoMovieAction).movieItem.voteUp();
            return {
                ...state
            };
        case TodoMoviesActionTypes.VOTE_DOWN:
            (action as VoteDownTodoMovieAction).movieItem.voteDown();
            return {
                ...state
            };
        case TodoMoviesActionTypes.ADD_MOVIE:
            return {
                ...state,
                items: [...state.items, (action as AddTodoMovieAction).movieItem]
            };
        case TodoMoviesActionTypes.ADD_TAG_COUNT:
            const count = (action as AddTagCountAction).tags;
            console.log('TAG COUNT -> ', count);
            return {
                ...state,
                tagCount: count
            };
    }
    return state;
}


