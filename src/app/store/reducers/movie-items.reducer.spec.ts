import { reducerTodoMovies, initializeTodoMoviesState } from './movie-items.reducer';
import { TodoMovieItem } from './../../models/todo-movie-item.models';
import {
    InitDataTodoMovieAction,
    AddTodoMovieAction,
    AddTagCountAction,
    VoteDownTodoMovieAction,
    VoteUpTodoMovieAction } from './../actions/movies-items.actions';

describe('reducerTodoMovies', () => {
    it('should init data movies', () => {
        const items: TodoMovieItem[] = [
            new TodoMovieItem('test1', new Date(), ''),
            new TodoMovieItem('test2', new Date(), ''),
            new TodoMovieItem('test3', new Date(), ''),
            new TodoMovieItem('test4', new Date(), ''),
        ];
        let state = initializeTodoMoviesState;
        state = reducerTodoMovies(state, new InitDataTodoMovieAction(items));

        expect(state.items.length).toBe(4);
    });
    it('should add a movie', () => {
        const item: TodoMovieItem = new TodoMovieItem('test1', new Date(), '');
        let state = initializeTodoMoviesState;
        state = reducerTodoMovies(state, new AddTodoMovieAction(item));

        expect(state.items.length).toBe(1);
    });
    it('should vote up the movie', () => {
        const item: TodoMovieItem = new TodoMovieItem('test1', new Date(), '');
        let state = initializeTodoMoviesState;
        state = reducerTodoMovies(state, new AddTodoMovieAction(item));
        state = reducerTodoMovies(state, new VoteUpTodoMovieAction(item));

        expect((state.items[0] as TodoMovieItem).votes).toBe(1);
    });
    it('should vote down the movie', () => {
        const item: TodoMovieItem = new TodoMovieItem('test1', new Date(), '');
        let state = initializeTodoMoviesState;
        state = reducerTodoMovies(state, new AddTodoMovieAction(item));
        state = reducerTodoMovies(state, new VoteDownTodoMovieAction(item));

        expect((state.items[0] as TodoMovieItem).votes).toBe(-1);
    });
    it('should add new tag count', () => {
        const count = 1000;
        let state = initializeTodoMoviesState;
        state = reducerTodoMovies(state, new AddTagCountAction(count));

        expect(state.tagCount).toBe(count);
    });
});


