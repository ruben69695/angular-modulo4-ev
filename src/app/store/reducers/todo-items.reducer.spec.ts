
import { TodoItemsState, initializeTodoItems, reducerTodoItems } from './todo-items.reducer';
import { TodoItem } from 'src/app/models/todo-item.models';
import { NewTodoItemAction, DeleteTodoItemsAction } from '../actions/todo-items.actions';

describe('initializeTodoItems', () => {
    it('should initialize items property with an empty array', () => {
        const state = initializeTodoItems;
        const empty: TodoItem[] = [];
        expect(state.items).toEqual(empty);
    });
});

describe('reducerTodoItems', () => {
    it('should add new todo item', () => {
        const item = new TodoItem('item', new Date(), '');
        const action = new NewTodoItemAction(item);
        let todoItemState = initializeTodoItems;
        todoItemState = reducerTodoItems(todoItemState, action);

        expect(todoItemState.items[0]).toEqual(item);
    });
    it('should delete todo items', () => {
        const item1 = new TodoItem('item 1', new Date(), '');
        const item2 = new TodoItem('item 2', new Date(), '');
        const deleteAction = new DeleteTodoItemsAction();
        let todoItemState = initializeTodoItems;

        // Select this two items, the action will delete all the selected items
        item1.select();
        item2.select();

        // Add items
        todoItemState = reducerTodoItems(todoItemState, new NewTodoItemAction(item1));
        todoItemState = reducerTodoItems(todoItemState, new NewTodoItemAction(item2));

        // Delete selected items
        todoItemState = reducerTodoItems(todoItemState, deleteAction);
        expect(todoItemState.items.length).toEqual(0);
    });
});




