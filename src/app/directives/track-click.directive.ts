import { Directive, ElementRef } from '@angular/core';
import { fromEvent } from 'rxjs';
import { TodoMoviesApiClient } from '../models/todo-movies-api-client.model';

@Directive({
  selector: '[appTrackClick]'
})
export class TrackClickDirective {
  private element: HTMLInputElement;
  constructor(private elRef: ElementRef, private moviesApi: TodoMoviesApiClient) {
    this.element = elRef.nativeElement;
    fromEvent(this.element, 'click').subscribe(event => this.track(event));
  }
  track(event: Event): void {
    const tags = this.element.attributes.getNamedItem('data-tracker-tags').value.split(' ');
    console.log(`||||||||||| track evento: "${tags}"`);
    this.moviesApi.incrementTag(tags.length);
  }

}
