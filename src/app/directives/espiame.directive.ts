import { Directive, OnInit, OnDestroy } from '@angular/core';

@Directive({
  selector: '[appEspiame]'
})
export class EspiameDirective implements OnInit, OnDestroy {
  static initNextId = 0;
  static destroyNextId = 0;
  constructor() { }
  ngOnInit(): void {
    console.log(`OnInit, evento llamado por ${++EspiameDirective.initNextId} vez`);
  }
  ngOnDestroy(): void {
    console.log(`OnDestroy, evento llamado por ${++EspiameDirective.destroyNextId} vez`);
  }
}
