import { TodoItem } from './todo-item.models';

export class TodoMovieItem extends TodoItem {
    votes: number;
    constructor(name: string, creationDate: Date, descr: string) {
        super(name, creationDate, descr);
        this.votes = 0;
    }

    voteUp(): void {
        this.votes++;
    }

    voteDown(): void {
        this.votes--;
    }
}
