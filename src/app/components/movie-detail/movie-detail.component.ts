import { Component, OnInit, InjectionToken, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TodoMoviesApiClient } from 'src/app/models/todo-movies-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/reducers/main-reducer';
import { TodoMovieItem } from 'src/app/models/todo-movie-item.models';
import { APP_CONFIG, AppConfig } from 'src/app/resources/app-config';

// interface AppConfig {
//   endPoint: string;
// }

// const APP_CONFIG_VALUE: AppConfig = {
//   endPoint: 'api.com'
// };

// const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

class BaseApi {
  getById(id: string): TodoMovieItem {
    console.log('hey');
    return null;
  }
}

class ApiDecorada extends BaseApi {
  constructor(@Inject(APP_CONFIG) private config: AppConfig, store: Store<AppState>) {
    super();
  }
  getById(id: string): TodoMovieItem {
    console.log('llamada por la clase decorada');
    console.log('config: ', this.config.endPoint);
    return null;
  }
}

// class TodoMoviesApiClientAntiguo {
//   getById(id: string): TodoMovieItem {
//     console.log('antiguo');
//     return null;
//   }
// }

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.css'],
  providers: [
    // { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
    { provide: BaseApi, useClass: ApiDecorada },
    // { provide: TodoMoviesApiClientAntiguo, useExisting: TodoMoviesApiClient }
  ]
})

export class MovieDetailComponent implements OnInit {

  id: string;
  style = {
    sources: {
      world: {
        type: 'geojson',
        data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'
      }
    },
    version: 8,
    layers: [{
      id: 'countries',
      type: 'fill',
      source: 'world',
      layout: {},
      paint: {
        'fill-color': '#6F788A'
      }
    }]
  };

  constructor(private route: ActivatedRoute, private api: BaseApi /*private apiAntigua: TodoMoviesApiClientAntiguo*/) {
    route.params.subscribe(params => { this.id = params['id']; });
    console.log(api.getById(this.id));
  }

  ngOnInit() {
  }

}
