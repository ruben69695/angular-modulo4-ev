import { Component, OnInit, HostBinding, Host, Input } from '@angular/core';
import { TodoMovieItem } from './../../models/todo-movie-item.models';
import { AppState } from 'src/app/store/reducers/main-reducer';
import { TodoMoviesApiClient } from 'src/app/models/todo-movies-api-client.model';

@Component({
  selector: 'app-todo-movie-item',
  templateUrl: './todo-movie-item.component.html',
  styleUrls: ['./todo-movie-item.component.css']
})
export class TodoMovieItemComponent implements OnInit {

  @HostBinding('attr.class') cssClass = 'col col-sm-3';
  @Input() movieItem: TodoMovieItem;

  constructor(private api: TodoMoviesApiClient) { }

  ngOnInit() {
  }

  voteUp(): boolean {
    this.api.voteUp(this.movieItem);
    return false;
  }

  voteDown(): boolean {
    this.api.voteDown(this.movieItem);
    return false;
  }

}
