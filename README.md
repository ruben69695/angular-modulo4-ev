# Especialización en desarrollo FullStack
Desarrollo FullStack por la universidad Austral, cursado en Coursera.

## Desarrollo de páginas con Angular - Evaluación del proyecto - Módulo 4

### Ejercicios
1. Registrar sucesos de interés durante el ciclo de vida de los componentes Angular.
2. Probar automáticamente el front-end de nuestra aplicación.
3. Probar automáticamente los diferentes componentes de nuestra aplicación Angular.

---

### Todo web application
Una aplicación web responsive desarrollada principalmente con el framework de Angular con una lista de tareas por hacer y peliculas pendientes por ver

### Para empezar

Para satisfacer las dependencias del proyecto angular, ejecute en la raíz del proyecto
```bash
npm install
```

Para satisfacer las dependencias del proyecto de la api ejecute
```bash
cd api/ && npm install
```

Para arrancar la api
```bash
node api/app.js
```

Para arrancar la aplicación
```bash
ng serve
```

Para arrancar los test
```bash
ng test
```

### Tecnologías
- HTML / CSS / JS / Bootstrap 4
- Angular / Redux / NodeJS / TypeScript
- Jasmine / Cypress / Karma
- Dexie / Mapbox
- Visual Studio Code / Git
- Bitbucket / Circleci